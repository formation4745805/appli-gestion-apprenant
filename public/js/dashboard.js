    //Declaration
//navBar
let navBarAccueil = document.getElementById('navBarAccueil');
let navBarPromotions = document.getElementById('navBarPromotions');
//section
let accueilBox = document.getElementById('accueilBox');
let promotionsBox = document.getElementById('promotionsBox');
let newPromoBox = document.getElementById('newPromo');
let updatePromoBox = document.getElementById('updatePromo');
let detailPromoBox = document.getElementById('detailPromo');
let newUserBox =document.getElementById('newUser');
//boutton et lien
let addPromo = document.getElementById('addPromo');
let promoRetour = document.getElementById('promoRetour');
let promoRetourUpdate = document.getElementById('promoRetourUpdate');
let promoSave = document.getElementById('promoSave');
let promoSaveUpdate = document.getElementById('promoSaveUpdate');
let promoDeleteUpdate = document.getElementById('promoDeleteUpdate');
let promoToDetail = document.querySelectorAll('.promoToDetail');
let promoToUpdate = document.querySelectorAll('.promoToUpdate');
let promoToDelete = document.querySelectorAll('.promoToDelete');
//zone d'écriture
let listAllPromo = document.getElementById('listAllPromo');




    //Fonctions de factorisation
function navBarOn(navBar) {
    navBar.classList.remove('border-b-2', 'text-blue-600');
    navBar.classList.add('border-l-2', 'border-t-2', 'border-r-2');
};

function navBarOff(navBar) {
    navBar.classList.add('border-b-2', 'text-blue-600');
    navBar.classList.remove('border-l-2', 'border-t-2', 'border-r-2');
};

function boxOn(box) {
    box.classList.remove('hidden');
};

function boxOff(box) {
    box.classList.add('hidden');
};

function catchDataSection(section) {
    let inputs = document.querySelectorAll(`#${section} input`);
    let data = {};
    for (input of inputs) {
        if (input.name !== "loginButton") {
            let key = input.name;
            let value = input.value;
            data[key] = value;
        }
    }
    return data;
}




    //switch des navBar
navBarPromotions.addEventListener('click', (e)=>{
    navBarOn(navBarPromotions);
    navBarOff(navBarAccueil);
    boxOn(promotionsBox);
    boxOff(newPromoBox);
    boxOff(accueilBox);
    window.history.pushState({}, "", HOME_URL+'dashboard/promotions');
});

navBarAccueil.addEventListener('click', (e)=>{
    navBarOn(navBarAccueil);
    navBarOff(navBarPromotions);
    boxOn(accueilBox);
    boxOff(newPromoBox);
    boxOff(promotionsBox);
    boxOff(updatePromoBox);
    boxOff(detailPromoBox);
    boxOff(newUserBox);
    window.history.pushState({}, "", HOME_URL+'dashboard');
});





    //navigation dans section
            //PROMOTION
        //ajouter
addPromo.addEventListener('click', (e)=>{
    e.preventDefault();
    window.history.pushState({}, "", HOME_URL+'dashboard/promotions/new');
    boxOn(newPromoBox);
    boxOff(promotionsBox);
});
        //Retour depuis ajouter vers liste
promoRetour.addEventListener('click', (e)=> {
    e.preventDefault();
    window.history.pushState({}, "", HOME_URL+'dashboard/promotions');
    boxOn(promotionsBox);
    boxOff(newPromoBox);
});

        //update
promoToUpdate.forEach(promo => {
    promo.addEventListener('click', (e)=> {
        e.preventDefault();
        let promoId = promo.getAttribute('href');
        window.history.pushState({}, "", HOME_URL+'dashboard/promotions/update');
        boxOff(promotionsBox);
        boxOn(updatePromoBox);
            //enregistrer les changements
        promoSaveUpdate.addEventListener('click', (e)=> {
            e.preventDefault();
            updateThisPromo(promoId);
        });
            //supprimer depuis update
        promoDeleteUpdate.addEventListener('click', (e)=> {
            e.preventDefault();
            deleteThisPromo(promoId);
        });
    });
});
async function updateThisPromo(id){
    let dataInput = catchDataSection('updatePromo');
    dataInput['PromoId'] = id ;
    let url = HOME_URL + 'updatePromo';

    await fetch(url, {
        method: "POST",
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify(dataInput)
    }).then(response => {
        if(response.status == 200){
            window.history.pushState({}, "", HOME_URL+'dashboard/promotions');
            return response.json()
        }
    }).then(data => {
        boxOn(promotionsBox);
        boxOff(updatePromoBox);
        let rowModif = document.getElementById(`promo${id}`);
        rowModif.innerHTML = `
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_Name}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_Start}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_End}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_Places}</td>
            <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToDetail" href="${data[0].Promo_Id}">voir</a></td>
            <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToUpdate" href="${data[0].Promo_Id}">modifier</a></td>
            <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToDelete" href="${data[0].Promo_Id}">supprimer</a></td>
        `;
    })
}
    //retour depuis update vers liste
promoRetourUpdate.addEventListener('click', (e)=> {
    e.preventDefault();
    window.history.pushState({}, "", HOME_URL+'dashboard/promotions');
    boxOn(promotionsBox);
    boxOff(updatePromoBox);
})

        //delete
promoToDelete.forEach(promo => {
    promo.addEventListener('click', (e)=> {
        e.preventDefault();
        let promoId = promo.getAttribute('href');
        deleteThisPromo(promoId);
    });
});
async function deleteThisPromo(id){
    let url = HOME_URL + 'deletePromo';
    let deleteId = ['id', id]
    await fetch(url, {
        method: "POST",
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify(deleteId)
    }).then(response => {
        if(response.status == 200){
            window.history.pushState({}, "", HOME_URL+'dashboard/promotions');
            return response.json();
        }
    }).then(data => {
        boxOn(promotionsBox);
        boxOff(updatePromoBox);
        let rowToDelete = document.getElementById(`promo${id}`);
        rowToDelete.remove();
    })
}

    //ajout d'une promotion
promoSave.addEventListener('click', (e)=> {
    e.preventDefault();
    fetchPromo();
});
async function fetchPromo(){
    let dataInput = catchDataSection('newPromo');
    let url = HOME_URL + 'addPromo';

    await fetch(url, {
        method: "POST",
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify(dataInput)
    }).then(response => {
        if(response.status == 200){
            window.history.pushState({}, "", HOME_URL+'dashboard/promotions');
            return response.json()
        }
    }).then(data => {
        boxOn(promotionsBox);
        boxOff(newPromoBox);
        listAllPromo.innerHTML += `
        <tr id="promo${data[0].Promo_Id}">
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_Name}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_Start}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_End}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].Promo_Places}</td>
            <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToDetail" href="${data[0].Promo_Id}">voir</a></td>
            <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToUpdate" href="${data[0].Promo_Id}">modifier</a></td>
            <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToDelete" href="${data[0].Promo_Id}">supprimer</a></td>
        </tr>
        `;
    });
}

//details d'une PROMO et début de chemin USER
promoToDetail.forEach(promo => {
    promo.addEventListener('click', (e)=> {
        e.preventDefault();
        let promoId = promo.getAttribute('href');
        boxOff(promotionsBox);
        boxOn(detailPromoBox);

        detailThisPromo(promoId);
    });
});
async function detailThisPromo(id) {

    let url = HOME_URL + 'listUser';
    let dataInput = {'PromoId': id};

    await fetch(url, {
        method: "POST",
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify(dataInput)
    }).then(response => {
        if(response.status == 200){
            window.history.pushState({}, "", HOME_URL+`dashboard/promotions/detail/${id}`);
            return response.json()
        };
    }).then(data => {
        detailPromoBox.innerHTML = `
        <div class="flex justify-between">
            <div class="text-left">
                <h2 class="text-lg font-bold mb-6">Promotion ${data[0].Promo_Name}</h2>
                <p>Information générale de la ${data[0].Promo_Name}</p>
            </div>
            <div class="text-right mr-6">
                <a id="addUser" href="${id}" class="bg-green-500 hover:bg-green-800 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Ajouter apprenant</a>
            </div>
        </div>
        <div class="flex justify-start items-center ml-6 mt-12">
            <button id="navBarApprenant" class="navBarApprenant w-24 h-8 border-l-2 border-t-2 border-r-2">Tableau apprenant</button> 
            <button id="navBarRetard" class="navBarRetard border-b-2 text-blue-600 w-24 h-8">Retard</button>
            <div class="border-b-2 flex-grow h-8"></div>
        </div>
        <div class="mr-6">
            <table class="table-auto w-full border-collapse">
                <thead>
                    <tr>
                        <th class="border-b-2 border-black px-4 py-2 text-left">Nom de famille</th>
                        <th class="border-b-2 border-black px-4 py-2 text-left">Prénom</th>
                        <th class="border-b-2 border-black px-4 py-2 text-left">Mail</th>
                        <th class="border-b-2 border-black px-4 py-2 text-left">Compte Activé</th>
                        <th class="border-b-2 border-black px-4 py-2 text-left"> Rôle</th>
                        <th class="border-b-2 border-black px-4 py-2 text-right"></th>
                        <th class="border-b-2 border-black px-4 py-2 text-right"></th>
                    </tr>
                </thead>
                <tbody id="listAllUser">
                    ${data.map(user => `
                        <tr id="user${user.User_Id}">
                            <td class="border-b-2 border-gray-300 px-4 py-2">${user.User_LastName}</td>
                            <td class="border-b-2 border-gray-300 px-4 py-2">${user.User_FirstName}</td>
                            <td class="border-b-2 border-gray-300 px-4 py-2">${user.User_Email}</td>
                            <td class="border-b-2 border-gray-300 px-4 py-2">${user.User_Activate === 1 ? 'oui' : 'non'}</td>
                            <td class="border-b-2 border-gray-300 px-4 py-2">${user.Role_Name}</td>
                            <td class="border-b-2 border-gray-300 px-4 py-2 text-right"><a class="userToUpdate" href="${user.User_Id}">éditer</a></td>
                            <td class="border-b-2 border-gray-300 px-4 py-2 text-right"><a class="userToDelete" href="${user.User_Id}">supprimer</a></td>
                        </tr>
                    `).join('')}
                </tbody>
            </table>
        </div>
        `;
        document.querySelector('footer').innerHTML += `<script src="`+HOME_URL+`js/user.js" defer></script>`;
        let scripts = document.querySelector('footer').querySelectorAll('script');
            for (let i = 0; i < scripts.length; i++) {
                if(!scripts[i].innerText){
                    const script = document.createElement('script');
                    script.src = scripts[i].src;
                    document.querySelector('footer').removeChild(scripts[i]);
                    document.querySelector('footer').appendChild(script);
                }
            }
    })
}