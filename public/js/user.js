        // début du chemin USER
    //déclaration
//zone d'écriture
let listAllUser = document.getElementById('listAllUser');
//navBar
let navBarApprenant = document.getElementById('navBarApprenant');
let navBarRetard = document.getElementById('navBarRetard');
//switch des navBar
navBarRetard.addEventListener('click', (e)=> {
    navBarOn(navBarRetard);
    navBarOff(navBarApprenant);
});

navBarApprenant.addEventListener('click', (e)=> {
    navBarOn(navBarApprenant);
    navBarOff(navBarRetard);
})
//boutton
let addUserButton = document.getElementById('addUser');
let userRetourNew = document.getElementById('userRetourNew');
let userSaveNew = document.getElementById('userSaveNew');

//vers ajout User
addUserButton.addEventListener('click', (e)=> {
    e.preventDefault();
    let promoId = addUserButton.getAttribute('href');
    boxOn(newUserBox);
    boxOff(detailPromoBox);
    window.history.pushState({}, "", HOME_URL+`dashboard/promotions/detail/${promoId}/newApprenant`);
    //retour vers liste user
    userRetourNew.addEventListener('click', (e)=> {
        e.preventDefault();
        boxOn(detailPromoBox);
        boxOff(newUserBox);
        window.history.pushState({}, "", HOME_URL+`dashboard/promotions/detail/${promoId}`);
    });
    userSaveNew.addEventListener('click', (e)=> {
        e.preventDefault();
        fetchUser(promoId);
    });
});
async function fetchUser(id) {

    let dataInput = catchDataSection('newUser');
    dataInput['PromoId'] = id ;
    let url = HOME_URL + 'addUser';

    await fetch(url, {
        method: "POST",
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify(dataInput)
    }).then(response => {
        if(response.status == 200){
            window.history.pushState({}, "", HOME_URL+`dashboard/promotions/detail/${id}`);
            return response.json()
        }
    }).then(data => {
        boxOn(detailPromoBox);
        boxOff(newUserBox);
        let roleName = '';
        let activeState = '';
        if(data[0].User_Activate == 0) {
            activeState = 'non';
        } else {
            activeState = 'oui';
        }
        switch (data[0].Role_Id) {
            case 1:
                roleName = "admninistrateur"
                break;
            case 2:
                roleName = "campus manager"
                break;
            case 3:
                roleName = "responsable pédagogique"
                break;
            case 4:
                roleName = "formateur"
                break;
            case 5:
                roleName = "délégué"
                break;
            case 6:
                roleName = "apprenant"
                break;
            default:
                break;
        }
        listAllUser.innerHTML += `
        <tr id="user${data[0].User_Id}">
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].User_LastName}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].User_FirstName}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${data[0].User_Email}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${activeState}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2">${roleName}</td>
            <td class="border-b-2 border-gray-300 px-4 py-2 text-right"><a class="userToUpdate" href="${data[0].User_Id}">éditer</a></td>
            <td class="border-b-2 border-gray-300 px-4 py-2 text-right"><a class="userToDelete" href="${data[0].User_Id}">supprimer</a></td>
        </tr>
        `;
    })
}