let registerButton = document.getElementById('registerButton');
let registerPassword = document.getElementById('registerPassword').value;
let registerPassword2 = document.getElementById('registerPassword').value;
let errorRegister = document.getElementById('errorRegister');

registerButton.addEventListener('click', (e)=>{

    e.preventDefault();
    if (registerPassword === registerPassword2) {
        let urlParams = new URLSearchParams(window.location.search);
        const UserEmail = urlParams.get('log');

        let url = HOME_URL + 'register';
        let data = catchData();
        data ['UserEmail'] = UserEmail ;
        console.log(data);

        fetch(url, {
            method: "POST",
            headers: {
                "content-Type": "application/json"
            },
            body: JSON.stringify(data)
        }).then(response => {
            if(response.status == 200){
                window.history.pushState({}, "", HOME_URL+'dashboard');
                return response.text()
            }
        }).then(data => {
            if(data){
                document.body.innerHTML = data
                let scripts = document.querySelector('footer').querySelectorAll('script');
                for (let i = 0; i < scripts.length; i++) {
                    if(!scripts[i].innerText){
                        const script = document.createElement('script');
                        script.src = scripts[i].src;
                        document.querySelector('footer').removeChild(scripts[i]);
                        document.querySelector('footer').appendChild(script);
                    }
                }
            }
        })
    } else {
        errorRegister.classList.remove('hidden');
    }
});

function catchData() {
    let inputs = document.querySelectorAll('input');
    let data = {};
    for (input of inputs) {
        if (input.name !== "loginButton") {
            let key = input.name;
            let value = input.value;
            data[key] = value;
        }
    }
    return data;
}