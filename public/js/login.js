let loginButton = document.getElementById('loginButton');

loginButton.addEventListener('click', (e)=>{

    e.preventDefault();

    let url = HOME_URL + 'login';
    let data = catchData();

    fetch(url, {
        method: "POST",
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify(data)
    }).then(response => {
        if(response.status == 200){
            window.history.pushState({}, "", HOME_URL+'dashboard');
            return response.text()
        }
    }).then(data => {
        if(data){
            document.body.innerHTML = data
            let scripts = document.querySelector('footer').querySelectorAll('script');
            for (let i = 0; i < scripts.length; i++) {
                if(!scripts[i].innerText){
                    const script = document.createElement('script');
                    script.src = scripts[i].src;
                    document.querySelector('footer').removeChild(scripts[i]);
                    document.querySelector('footer').appendChild(script);
                }
            }
        }
    })
});

function catchData() {
    let inputs = document.querySelectorAll('input');
    let data = {};
    for (input of inputs) {
        if (input.name !== "loginButton") {
            let key = input.name;
            let value = input.value;
            data[key] = value;
        }
    }
    return data;
}