<?php

namespace src\Controllers;

use src\Models\Database;
use src\Models\Promo;
use src\Repositories\PromoRepository;
use src\Services\Reponse;
use src\Services\Securite;

class PromoController {

    use Reponse;
    use Securite;

    public function newPromo() {
        $data = !empty($_POST) ? self::sanitize($_POST) : self::sanitize(json_decode(file_get_contents("php://input")));
        $newPromo = new Promo($data);
        $database = new Database();
        $PromoRepository = new PromoRepository($database);
        $retour = $PromoRepository->createPromoWithReturn($newPromo);
        $this->sendJson($retour, 200);
    }

    public function deletePromo() {
        $data = !empty($_POST) ? self::sanitize($_POST) : self::sanitize(json_decode(file_get_contents("php://input")));
        $id = (int)$data[1];
        $database = new Database();
        $PromoRepository = new PromoRepository($database);
        $retour = $PromoRepository->deletePromo($id);
        if($retour){
            $this->sendJson($data, 200);
        } else {
            echo json_encode("Pas delete");
        }
    }

    public function updatePromo() {
        $data = !empty($_POST) ? self::sanitize($_POST) : self::sanitize(json_decode(file_get_contents("php://input")));
        $newPromo = new Promo($data);
        $database = new Database();
        $PromoRepository = new PromoRepository($database);
        $retour = $PromoRepository->updatePromoWithReturn($newPromo);
        $this->sendJson($retour, 200);
    }

    public function allUserOfThisPromo() {
        $data = !empty($_POST) ? self::sanitize($_POST) : self::sanitize(json_decode(file_get_contents("php://input")));
        $id = (int)$data['PromoId'];
        $database = new Database();
        $PromoRepository = new PromoRepository($database);
        $retour = $PromoRepository->allUserOfThisPromo($id);
        $this->sendJson($retour, 200);
    }
}