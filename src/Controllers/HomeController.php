<?php

namespace src\Controllers;

use src\Models\Database;
use src\Models\User;
use src\Repositories\PromoRepository;
use src\Repositories\UserRepository;
use src\Services\Reponse;
use src\Services\Securite;

class HomeController {

    use Reponse;
    use Securite;

    public function index():void {
        if(isset($_GET['error'])) {
            $error = htmlspecialchars($_GET['error']);
        } else {
            $error = '';
        }
        $this->render("writeHere", ["error"=>$error]);
    }

    public function register():void {
        if(isset($_GET['error'])) {
            $error = htmlspecialchars($_GET['error']);
        } else {
            $error = '';
        }
        $this->render("accueil", ["error"=>$error]);
    }

    public function quit():void {
        session_destroy();
        header("location: ".HOME_URL);
        die();
    }

    public function page404():void {
        header("HTTP/1.1 404 Not Found");
        $this->render('404');
    }

    public function pageDashboard() :void {
        $erreur = isset($_GET["erreur"]) ? $_GET["erreur"] : '';
        $user = unserialize($_SESSION['user']);
        $this->render("dashboard", ["erreur" => $erreur, "user" => $user]);
    }

    public function login(){
        $data = file_get_contents("php://input");
        $user = (json_decode($data, true));
        if(!empty($user)) {
              $obj = new User($user);
              $mail = $obj->getUserEmail();
              if(isset($mail) && !empty($mail)){
                    $mail = htmlspecialchars($mail);
              }
              $password = $obj->getUserPassword();
              if(isset($password) && !empty($password)){
                    $password = hash("whirlpool", $password);
              }
              $DbConnexion = new Database();
              $UserRepository = new UserRepository($DbConnexion);
              if($UserRepository->login($mail, $password)){
                    $user = $UserRepository->getThisUserByMail($mail);
                    $_SESSION["connected"] = TRUE;
                    $_SESSION["user"] = serialize($user);
                    $this->render("dashboard",["user" => $user], 200);
                    die();
              } else{
                    $code = 406;
                    $message = ["echec" => "Echec de connexion"];
                    $this->sendJson($message, $code);
                    die();
              }
        }
    }

    public function listPromo() {
        $DbConnexion = new Database();
        $PromoRepository = new PromoRepository($DbConnexion);
        $allPromos = $PromoRepository->getAllPromo();
        $this->sendJson($allPromos, 200);
        die();
    }

    public function updateAndLog() {
        $data = !empty($_POST) ? self::sanitize($_POST) : self::sanitize(json_decode(file_get_contents("php://input")));
        $UserEmail = $data['UserEmail'];
        $UserPassword = hash("whirlpool",$data['UserPassword']);
        $database = new Database();
        $UserRepository = new UserRepository($database);
        if($UserRepository->validateUser($UserPassword, $UserEmail)){
            if($UserRepository->login($UserEmail, $UserPassword)){
                $user = $UserRepository->getThisUserByMail($UserEmail);
                $_SESSION["connected"] = TRUE;
                $_SESSION["user"] = serialize($user);
                $this->render("dashboard",["user" => $user], 200);
                die();
            } else{
                $code = 406;
                $message = ["echec" => "Echec de connexion"];
                $this->sendJson($message, $code);
                die();
            }
        } 
    }
}