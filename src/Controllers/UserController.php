<?php

namespace src\Controllers;

use src\Models\Database;
use src\Models\User;
use src\Repositories\UserRepository;
use src\Services\Reponse;
use src\Services\Securite;

class UserController {

    use Reponse;
    use Securite;

    public function newUser() {
        $data = !empty($_POST) ? self::sanitize($_POST) : self::sanitize(json_decode(file_get_contents("php://input")));
        $PromoId = (int)$data['PromoId'];
        $newUser = new User($data);
        $database = new Database();
        $UserRepository = new UserRepository($database);
        if($UserRepository->getThisUserByMail($newUser->getUserEmail())){
            $this->sendJson(['erreur'=>'Cet email existe déjà'], 403);
        } else {
            $retour = $UserRepository->createUserWithReturn($newUser, $PromoId);
            if($retour){
                $this->sendJson($retour, 200);
                $to = $newUser->getUserEmail();
                $subject = "Votre inscription à Simplon!";
                $message = "
                    Merci de cliquer sur le lien suivant pour finaliser votre inscription:
                    http://brief6appligestion/".HOME_URL."validate?log=".$newUser->getUserEmail()."
                    Félicitation!!!";
                $headers = 'From: email@envoi.fr' . "\r\n" .
                            'Reply-To: email@envoi.fr' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                $test = mail($to, $subject, $message, $headers);
            }
        }
    }
}