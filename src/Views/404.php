<?php

include_once __DIR__ . '/Includes/header.php';

?>
<div class="main">
    <h1>Page introuvable...</h1>

    <button class="center" onclick="location.href='<?=HOME_URL?>'">Retourner à l'accueil</button>

</div>
<?php

include_once __DIR__ . '/Includes/footer.php';