<?php

use src\Models\Database;
use src\Repositories\PromoRepository;
use src\Repositories\UserRepository;

$database = new Database();
$PromoRepository = new PromoRepository($database);
$listPromo = $PromoRepository->getAllPromo();
$UserRepository = new UserRepository($database);
$listUser = $UserRepository->getAllUser();
?>

<div class="flex justify-start items-center ml-6 mt-12">
    <button id="navBarAccueil" class="navBarAccueil w-24 h-8 border-l-2 border-t-2 border-r-2">Accueil</button> 
    <button id="navBarPromotions" class="navBarPromotions border-b-2 text-blue-600 w-24 h-8">Promotions</button>
    <div class="border-b-2 flex-grow h-8"></div>
</div>

<section id="accueilBox" class="ml-6 mt-12">
    <h2>Bonjour Accueil</h2>
</section>

<section id="promotionsBox" class="ml-6 mt-12 hidden">
    <div class="flex justify-between">
        <div class="text-left">
            <h2 class="text-lg font-bold mb-6">Bonjour Promotions</h2>
            <p>Tableau des promotions de Simplon</p>
        </div>
        <div class="text-right mr-6">
            <button id="addPromo" class="bg-green-500 hover:bg-green-800 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Ajouter promotion</button>
        </div>
    </div>
    <div class="mr-6">
        <table class="table-auto w-full border-collapse">
            <thead>
                <tr>
                    <th class="border-b-2 border-black px-4 py-2 text-left">Promotion</th>
                    <th class="border-b-2 border-black px-4 py-2 text-left">Début</th>
                    <th class="border-b-2 border-black px-4 py-2 text-left">Fin</th>
                    <th class="border-b-2 border-black px-4 py-2 text-left">Place</th>
                    <th class="border-b-2 border-black px-4 py-2 text-right"></th>
                    <th class="border-b-2 border-black px-4 py-2 text-right"></th>
                    <th class="border-b-2 border-black px-4 py-2 text-right"></th>
                </tr>
            </thead>
            <tbody id="listAllPromo">
                    <?php
                    foreach ($listPromo as $promo) { ?>
                <tr id="promo<?= $promo->getPromoId() ?>">
                    <td class="border-b-2 border-gray-300 px-4 py-2"><?= $promo->getPromoName() ?></td>
                    <td class="border-b-2 border-gray-300 px-4 py-2"><?= $promo->getPromoStart() ?></td>
                    <td class="border-b-2 border-gray-300 px-4 py-2"><?= $promo->getPromoEnd() ?></td>
                    <td class="border-b-2 border-gray-300 px-4 py-2"><?= $promo->getPromoPlaces() ?></td>
                    <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToDetail" href="<?= $promo->getPromoId() ?>">voir</a></td>
                    <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToUpdate" href="<?= $promo->getPromoId() ?>">modifier</a></td>
                    <td class=" border-b-2 border-gray-300 px-4 py-2 text-right"><a class="promoToDelete" href="<?= $promo->getPromoId() ?>">supprimer</a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</section>

<section id="newPromo" class="ml-6 mt-12 hidden">
    <div class="text-left mb-12">
        <h2 class="text-2xl font-bold mb-6">Création d'une Promotion</h2>
    </div>
    <div class="form flex flex-col mx-[5%] justify-center h-1/2 bg-gray-200 p-6">
        <label class="mt-4 ml-4 text-left text-xl"> Nom de la promotion</label>
        <input class="mx-4 h-8 mt-2" type="text" name="PromoName">
        <label class="mt-4 ml-4 text-left text-xl"> Date de début</label>
        <input class="mx-4 h-8 mt-2" type="date" name="PromoStart">
        <label class="mt-4 ml-4 text-left text-xl"> Date de fin</label>
        <input class="mx-4 h-8 mt-2" type="date" name="PromoEnd">
        <label class="mt-4 ml-4 text-left text-xl"> Place(s) disponble(s)</label>
        <input class="mx-4 h-8 mt-2" type="number" name="PromoPlaces">
        <div class="flex justify-between mt-12">
            <button id="promoRetour" class="mt-16 ml-12 bg-blue-400 hover:bg-blue-600 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Retour</button>
            <button id="promoSave" class="mt-16 mr-12 bg-blue-400 hover:bg-blue-600 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Sauvegarder</button>
        </div>
    </div>
</section>

<section id="updatePromo" class="ml-6 mt-12 hidden">
    <div class="text-left mb-12">
        <h2 class="text-2xl font-bold mb-6">Création d'une Promotion</h2>
    </div>
    <div class="flex flex-col mx-[5%] justify-center h-1/2 bg-gray-200 p-6">
        <label class="mt-4 ml-4 text-left text-xl"> Nom de la promotion</label>
        <input class="mx-4 h-8 mt-2" type="text" name="PromoName">
        <label class="mt-4 ml-4 text-left text-xl"> Date de début</label>
        <input class="mx-4 h-8 mt-2" type="date" name="PromoStart">
        <label class="mt-4 ml-4 text-left text-xl"> Date de fin</label>
        <input class="mx-4 h-8 mt-2" type="date" name="PromoEnd">
        <label class="mt-4 ml-4 text-left text-xl"> Place(s) disponble(s)</label>
        <input class="mx-4 h-8 mt-2" type="number" name="PromoPlaces">
        <div class="flex justify-between mt-12">
            <button id="promoRetourUpdate" class="mt-16 ml-12 bg-blue-400 hover:bg-blue-600 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Retour</button>
            <div>
                <button id="promoDeleteUpdate" class="mt-16 mr-12 bg-red-400 hover:bg-red-600 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Supprimer</button>
                <button id="promoSaveUpdate" class="mt-16 mr-12 bg-blue-400 hover:bg-blue-600 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Sauvegarder</button>
            </div>
        </div>
    </div>
</section>

<section id="detailPromo" class="ml-6 mt-12 hidden">
</section>

<section id="newUser" class="ml-6 mt-12 hidden">
    <div class="text-left mb-12">
        <h2 class="text-2xl font-bold mb-6">Création d'un Apprenant</h2>
    </div>
    <div class="flex flex-col mx-[5%] justify-center h-1/2 bg-gray-200 p-6">
        <label class="mt-4 ml-4 text-left text-xl"> Nom </label>
        <input class="mx-4 h-8 mt-2" type="text" name="UserLastName">
        <label class="mt-4 ml-4 text-left text-xl"> Prénom </label>
        <input class="mx-4 h-8 mt-2" type="text" name="UserFirstName">
        <label class="mt-4 ml-4 text-left text-xl"> Email</label>
        <input class="mx-4 h-8 mt-2" type="email" name="UserEmail">
        <div class="flex justify-between mt-12">
            <button id="userRetourNew" class="mt-16 ml-12 bg-blue-400 hover:bg-blue-600 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Retour</button>
            <button id="userSaveNew" class="mt-16 mr-12 bg-blue-400 hover:bg-blue-600 text-white mx-auto my-6 w-36 items-center text-center text-sm font-bold py-2 rounded">Sauvegarder</button>
        </div>
    </div>
</section>
