<?php

use src\Models\Database;
use src\Repositories\PromoRepository;
use src\Repositories\UserRepository;

$dbConnexion = new Database;
$UserRepository = new UserRepository($dbConnexion);
$PromoRepository = new PromoRepository($dbConnexion);
$allPromos = $PromoRepository->getAllPromo();

?>


<div class="tabs tabs-lifted z-10 -mb-[var(--tab-border)] justify-self-start">
    <button class="navBarAccueil tab tab-active [--tab-bg:var(--fallback-b1,oklch(var(--b1)))]">Accueil</button> 
    <button class="navBarPromotions tab [--tab-border-color:transparent]">Promotions</button> 
    <button class="navBarUtilisateurs tab [--tab-border-color:transparent]">Utilisateurs</button> 
    <div class="tab [--tab-border-color:transparent]"></div>
</div>

<section id="Accueil">

</section>

<section id="Promotions">
    <h2>Toutes les Promotions</h2>
    <caption>
        <p>Tableau des promotions de Simplon:</p>
    </caption>
        <tr>
            <th>Promotion</th>
            <th>Début</th>
            <th>Fin</th>
            <th>Places</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach($allPromos as $Promo) { ?>
        <tr>
            <td><?= $Promo->getPromoName() ?></td>
            <td><?= $Promo->getPromoStart() ?></td>
            <td><?= $Promo->getPromoEnd() ?></td>
            <td><?= $Promo->getPromoPlaces() ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php } ?>

</section>

<section id="Utilisateurs">

</section>