<?php

include_once __DIR__ . '/Includes/header.php';

?>

<div id="written" class="write m-l-4 m-t-12">

    <form action="<?= HOME_URL ?>login" method="POST" class="flex flex-col mx-[25%] mt-32 md:mx-[30%] md:mt-48 lg:mx-[35%] lg:mt-64 w-1/2 md:w-1/3 justify-center h-1/2 bg-gray-200 p-6">
        <h1 class="items-center text-center text-3xl">Bienvenue</h1>
        <label class="mt-4 ml-4 text-left text-xl"> Email *</label>
        <input name="UserEmail" class="mx-4 h-8 mt-2" type="email" id="loginEmail">
        <label class="mt-4 ml-4 text-left text-xl"> Mot de passe *</label>
        <input name="UserPassword" class="mx-4 h-8 mt-2" type="password" id="loginPassword">
        <input type="submit" name="loginButton" id="loginButton" class="bg-blue-500 hover:bg-blue-700 text-white mx-auto my-6 w-32 items-center text-center font-bold py-2 rounded" value="Connexion">
    </form>

</div>

<?php

include_once __DIR__ . '/Includes/footer.php';

?>