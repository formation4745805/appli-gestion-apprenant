-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 19 avr. 2024 à 11:53
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `appligestion`
--

-- --------------------------------------------------------

--
-- Structure de la table `appligestion_contient`
--

DROP TABLE IF EXISTS `appligestion_contient`;
CREATE TABLE IF NOT EXISTS `appligestion_contient` (
  `Promo_Id` int NOT NULL,
  `User_Id` int NOT NULL,
  PRIMARY KEY (`Promo_Id`,`User_Id`),
  KEY `AppliGestion_Contient_AppliGestion_User0_FK` (`User_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `appligestion_contient`
--

INSERT INTO `appligestion_contient` (`Promo_Id`, `User_Id`) VALUES
(24, 1),
(24, 4);

-- --------------------------------------------------------

--
-- Structure de la table `appligestion_lecon`
--

DROP TABLE IF EXISTS `appligestion_lecon`;
CREATE TABLE IF NOT EXISTS `appligestion_lecon` (
  `Lecon_Id` int NOT NULL AUTO_INCREMENT,
  `Lecon_Name` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `Lecon_Start` datetime NOT NULL,
  `Lecon_End` datetime NOT NULL,
  `Lecon_Code` int NOT NULL,
  `Promo_Id` int NOT NULL,
  PRIMARY KEY (`Lecon_Id`),
  KEY `AppliGestion_Lecon_AppliGestion_Promo_FK` (`Promo_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `appligestion_promo`
--

DROP TABLE IF EXISTS `appligestion_promo`;
CREATE TABLE IF NOT EXISTS `appligestion_promo` (
  `Promo_Id` int NOT NULL AUTO_INCREMENT,
  `Promo_Name` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `Promo_Start` date NOT NULL,
  `Promo_End` date NOT NULL,
  `Promo_Places` int NOT NULL,
  PRIMARY KEY (`Promo_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `appligestion_promo`
--

INSERT INTO `appligestion_promo` (`Promo_Id`, `Promo_Name`, `Promo_Start`, `Promo_End`, `Promo_Places`) VALUES
(24, 'DWWM2', '2024-01-08', '2024-10-10', 15);

-- --------------------------------------------------------

--
-- Structure de la table `appligestion_role`
--

DROP TABLE IF EXISTS `appligestion_role`;
CREATE TABLE IF NOT EXISTS `appligestion_role` (
  `Role_Id` int NOT NULL AUTO_INCREMENT,
  `Role_Name` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`Role_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `appligestion_role`
--

INSERT INTO `appligestion_role` (`Role_Id`, `Role_Name`) VALUES
(1, 'admninistrateur'),
(2, 'campus manager'),
(3, 'responsable pédagogique'),
(4, 'formateur'),
(5, 'délégué'),
(6, 'apprenant');

-- --------------------------------------------------------

--
-- Structure de la table `appligestion_take`
--

DROP TABLE IF EXISTS `appligestion_take`;
CREATE TABLE IF NOT EXISTS `appligestion_take` (
  `Lecon_Id` int NOT NULL,
  `User_Id` int NOT NULL,
  `Take_Late` tinyint(1) NOT NULL,
  `Take_Absent` tinyint(1) NOT NULL,
  PRIMARY KEY (`Lecon_Id`,`User_Id`),
  KEY `AppliGestion_Take_AppliGestion_User0_FK` (`User_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `appligestion_user`
--

DROP TABLE IF EXISTS `appligestion_user`;
CREATE TABLE IF NOT EXISTS `appligestion_user` (
  `User_Id` int NOT NULL AUTO_INCREMENT,
  `User_LastName` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `User_FirstName` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `User_Email` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `User_Activate` tinyint(1) NOT NULL,
  `User_Password` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `Role_Id` int NOT NULL,
  PRIMARY KEY (`User_Id`),
  KEY `AppliGestion_User_AppliGestion_Role_FK` (`Role_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `appligestion_user`
--

INSERT INTO `appligestion_user` (`User_Id`, `User_LastName`, `User_FirstName`, `User_Email`, `User_Activate`, `User_Password`, `Role_Id`) VALUES
(1, 'Simplon', 'Admin', 'admin@simplon.fr', 1, 'cc9f410d0e06619906a5c991daff942ad38c4f40cc12b30808a11e88833c5a6434342a01faae949aa0b02b655d5fc7f3397b96dad59a1744f74d044d93e12f66', 1),
(4, 'ROCHES', 'Laurent', 'rocheslaurent@gmail.com', 1, 'password', 6);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `appligestion_contient`
--
ALTER TABLE `appligestion_contient`
  ADD CONSTRAINT `AppliGestion_Contient_AppliGestion_Promo_FK` FOREIGN KEY (`Promo_Id`) REFERENCES `appligestion_promo` (`Promo_Id`),
  ADD CONSTRAINT `AppliGestion_Contient_AppliGestion_User0_FK` FOREIGN KEY (`User_Id`) REFERENCES `appligestion_user` (`User_Id`);

--
-- Contraintes pour la table `appligestion_lecon`
--
ALTER TABLE `appligestion_lecon`
  ADD CONSTRAINT `AppliGestion_Lecon_AppliGestion_Promo_FK` FOREIGN KEY (`Promo_Id`) REFERENCES `appligestion_promo` (`Promo_Id`);

--
-- Contraintes pour la table `appligestion_take`
--
ALTER TABLE `appligestion_take`
  ADD CONSTRAINT `AppliGestion_Take_AppliGestion_Lecon_FK` FOREIGN KEY (`Lecon_Id`) REFERENCES `appligestion_lecon` (`Lecon_Id`),
  ADD CONSTRAINT `AppliGestion_Take_AppliGestion_User0_FK` FOREIGN KEY (`User_Id`) REFERENCES `appligestion_user` (`User_Id`);

--
-- Contraintes pour la table `appligestion_user`
--
ALTER TABLE `appligestion_user`
  ADD CONSTRAINT `AppliGestion_User_AppliGestion_Role_FK` FOREIGN KEY (`Role_Id`) REFERENCES `appligestion_role` (`Role_Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
