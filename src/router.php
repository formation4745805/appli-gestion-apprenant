<?php

use src\Controllers\HomeController;
use src\Controllers\PromoController;
use src\Controllers\UserController;
use src\Services\Routing;

$route = $_SERVER['REDIRECT_URL'];
$methode = $_SERVER['REQUEST_METHOD'];

$HomeController = new HomeController;
$PromoController = new PromoController;
$UserController = new UserController;

$routeComposee = Routing::routeComposee($route);


switch ($route) {
    case HOME_URL:
        if(isset($_SESSION['connected']) && $methode == "POST") {
            header("Location :" .HOME_URL."dashboard");
            die();
        } else {
            $HomeController->index();
        }
        break;
    case HOME_URL.'validate':
        $HomeController->register();
        break;
    case HOME_URL.'register':
        $HomeController->updateAndLog();
        break;
    case HOME_URL.'login':
        if($methode == "POST"){
            $HomeController->login();
            die;
        } else {
            $HomeController->index();
        }
        break;
    case HOME_URL . 'deconnexion':
        $HomeController->quit();
        break;
    case HOME_URL.'dashboard':
        if(isset($_SESSION['connected']) && $methode = "GET"){
            $HomeController->pageDashboard();
            die;
        } else {
            $HomeController->index();
        }
        break;
    case HOME_URL.'addPromo':
        $PromoController->newPromo();
        break;
    case HOME_URL.'deletePromo':
        $PromoController->deletePromo();
        break;
    case HOME_URL.'updatePromo':
        $PromoController->updatePromo();
        break;
    case HOME_URL. 'listUser':
        $PromoController->allUserOfThisPromo();
        break;
    case HOME_URL . 'addUser':
        $UserController->newUser();
        break;
    default :
        $HomeController->index();
        break;
}
