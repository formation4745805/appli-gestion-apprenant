<?php

namespace src\Repositories;

use PDO;
use PDOException;
use src\Models\Database;
use src\Models\Take;
use src\Models\User;

class UserRepository {

    private $DB;

    public function __construct() {
        $database = new Database();
        $this->DB = $database->getDB();
        require_once __DIR__.'/../../config.php';
    }

    public function login(string $email, string $password) {

        $hash = hash("whirlpool", $password);

        try{
            $sql = "SELECT * FROM ".PREFIXE."User WHERE User_Email = :email AND User_Password = :password;";
            $statement = $this->DB->prepare($sql);
            $retour = $statement->execute([
                ":email" => $email,
                ":password" => $hash
            ]);
            return $retour;
        }catch (PDOException $error){
            //a changer plus tard
            var_dump($error);
        }
        while ($row = $retour->fetch(\PDO::FETCH_ASSOC)){
            $user = new User($row);
        }
        return $user;
    }

    public function getAllUser (): array {
        $sql = "SELECT * FROM ".PREFIXE."User;";
        $statement = $this->DB->prepare($sql);
        $statement->execute();
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, User::class);
        return $retour;
    }

    public function getThisUserById (int $id): User {
        $sql = "SELECT * FROM ".PREFIXE."User WHERE User_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":id" => $id
        ]);
        $statement->setFetchMode(PDO::FETCH_CLASS, User::class);
        $retour = $statement->fetch();
        return $retour;
    }

    public function getThisUserByMail (string $mail): User|bool {
        $sql = "SELECT * FROM ".PREFIXE."User WHERE User_Email = :mail;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":mail" => $mail
        ]);
        $statement->setFetchMode(PDO::FETCH_CLASS, User::class);
        $retour = $statement->fetch();
        return $retour;
    }

    public function createUser(User $user, int $promoId) {
        $password = hash("whirlpool", $user->getUserPassword());
        $sql = "INSERT INTO ".PREFIXE."User VALUES (NULL,:UserPassword,:UserFirstName,:UserLastName,:UserActivate,:UserEmail,:RoleId);
                INSERT INTO ".PREFIXE."Contient VALUES (:PromoId, LAST_INSERT_ID());";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":UserPassword" => $password,
            ":UserFirstName" => $user->getUserFirstName(),
            ":UserLastName" => $user->getUserLastName(),
            ":UserActivate" => $user->isUserActivate(),
            ":UserEmail" => $user->getUserEmail(),
            ":RoleId" => $user->getRoleId(),
            ":PromoId" => $promoId
        ]);
        return $retour;
    }

    public function createUserWithReturn(User $user, int $promoId): array {
        $password = hash("whirlpool", $user->getUserPassword());
    
        // Insertion des données de l'utilisateur
        $sql = "INSERT INTO ".PREFIXE."User VALUES (NULL,:UserLastName,:UserFirstName,:UserEmail,:UserActivate,:UserPassword,:RoleId)";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":UserLastName" => $user->getUserLastName(),
            ":UserFirstName" => $user->getUserFirstName(),
            ":UserEmail" => $user->getUserEmail(),
            ":UserActivate" => $user->isUserActivate(),
            ":UserPassword" => $password,
            ":RoleId" => $user->getRoleId()
        ]);

        $sql = "INSERT INTO ".PREFIXE."Contient VALUES (:PromoId, LAST_INSERT_ID())";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":PromoId" => $promoId
        ]);

        $sql = "SELECT * FROM ".PREFIXE."User WHERE User_Id = LAST_INSERT_ID()";
        $statement = $this->DB->query($sql);
        $retour = $statement->fetchAll(PDO::FETCH_ASSOC);
        
        return $retour;
    }

    public function updateUser (User $user) {
        $sql = "UPDATE ".PREFIXE."User
                    SET
                        User_Password = :password,
                        User_FirstName = :firstName,
                        User_LastName = :lastName,
                        User_Activate = :activate,
                        User_Email = :email,
                        Role_Id = :roleId
                    WHERE User_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":password" => $user->getUserPassword(),
            ":firstName" => $user->getUserFirstName(),
            ":lastName" => $user->getUserLastName(),
            ":activate" => $user->isUserActivate(),
            ":email" => $user->getUserEmail(),
            ":roleId" => $user->getRoleId(),
            ":id" => $user->getUserId()
        ]);
        return $retour;
    }

    public function validateUser (string $password, string $email) {
        $sql = "UPDATE ".PREFIXE."User
                    SET
                        User_Password = :password,
                        User_Activate = :activate
                    WHERE User_Email = :email;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":password" => $password,
            ":activate" => "1",
            ":email" => $email,
        ]);
        return $retour;
    }

    public function getLateCount(int $id): int {
        $sql = "SELECT COUNT(*) FROM ".PREFIXE."Take 
                WHERE User_Id = :id
                AND Take_Late = 1;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":id" => $id
        ]);
        return $retour;
    }

    public function getAllLateByUser(int $id): array {
        $sql = "SELECT * FROM ".PREFIXE."Take 
                WHERE User_Id = :id
                AND Take_Late = 1;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":id" => $id
        ]);
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, Take::class);
        return $retour;
    }

    public function getAllAbsentByUser(int $id): array {
        $sql = "SELECT * FROM ".PREFIXE."Take 
                WHERE User_Id = :id
                AND Take_Absent = 1;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":id" => $id
        ]);
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, Take::class);
        return $retour;
    }

    public function updateUserRole (int $roleId,int $id) {
        $sql = "UPDATE ".PREFIXE."User SET Role_Id = :roleId WHERE User_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":roleId" => $roleId,
            ":id" => $id
        ]);
        return $retour;
    }

    public function deleteThisUser(int $id) {
        $sql = "DELETE FROM ".PREFIXE."Valide WHERE User_Id = :id;
                DELETE FROM ".PREFIXE."Contient WHERE User_Id = :id;
                DELETE FROM ".PREFIXE."User WHERE User_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":id"=> $id
        ]);
        return $retour;
    }
}