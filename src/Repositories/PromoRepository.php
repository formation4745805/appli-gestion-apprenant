<?php

namespace src\Repositories;

use PDO;
use src\Models\Database;
use src\Models\Promo;

class PromoRepository {

    private $DB;

    public function __construct() {
        $database = new Database();
        $this->DB = $database->getDB();
        require_once __DIR__.'/../../config.php';
    }

    public function createPromo (Promo $Promo) {
        $sql = "INSERT INTO ".PREFIXE."Promo VALUES (NULL,:PromoName, :PromoStart, :PromoEnd, :PromoPlaces);";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":PromoName" => $Promo->getPromoName(),
            ":PromoStart" => $Promo->getPromoStart(),
            ":PromoEnd" => $Promo->getPromoEnd(),
            ":PromoPlaces" => $Promo->getPromoPlaces()
        ]);
        return $retour;
    }

    public function createPromoWithReturn(Promo $Promo) {
        $sql = "INSERT INTO ".PREFIXE."Promo VALUES (NULL,:PromoName, :PromoStart, :PromoEnd, :PromoPlaces)";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":PromoName" => $Promo->getPromoName(),
            ":PromoStart" => $Promo->getPromoStart(),
            ":PromoEnd" => $Promo->getPromoEnd(),
            ":PromoPlaces" => $Promo->getPromoPlaces()
        ]);

        $lastInsertId = $this->DB->lastInsertId();
        if (!$lastInsertId) {
            return null;
        }
        $sql = "INSERT INTO ".PREFIXE."Contient VALUES (LAST_INSERT_ID(), :UserId)";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":UserId" => '1'
        ]);

        $selectSql = "SELECT * FROM ".PREFIXE."Promo WHERE Promo_Id = :lastInsertId";
        $selectStatement = $this->DB->prepare($selectSql);
        $selectStatement->execute([":lastInsertId" => $lastInsertId]);

        return $selectStatement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllPromo(): array {
        $sql = "SELECT * FROM ".PREFIXE."Promo;";
        $statement = $this->DB->prepare($sql);
        $statement->execute();
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, Promo::class);
        return $retour;
    }

    public function getThisPromoById (int $id): Promo {
        $sql = "SELECT * FROM ".PREFIXE."Promo WHERE Promo_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":id" => $id
        ]);
        $statement->setFetchMode(PDO::FETCH_CLASS, Promo::class);
        $retour = $statement->fetch();
        return $retour;
    }

    public function updatePromo(Promo $Promo) {
        $sql = "UPDATE ".PREFIXE."Promo
            SET
                Promo_Name = :name,
                Promo_Start = :start,
                Promo_End = :end,
                Promo_Places = :places
            WHERE Promo_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":name" => $Promo->getPromoName(),
            ":start" => $Promo->getPromoStart(),
            ":end" => $Promo->getPromoEnd(),
            ":places" => $Promo->getPromoPlaces(),
            ":id" => $Promo->getPromoId()
        ]);
        return $retour;
    }

    public function updatePromoWithReturn(Promo $Promo) {
        $sql = "UPDATE ".PREFIXE."Promo
            SET
                Promo_Name = :name,
                Promo_Start = :start,
                Promo_End = :end,
                Promo_Places = :places
            WHERE Promo_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":name" => $Promo->getPromoName(),
            ":start" => $Promo->getPromoStart(),
            ":end" => $Promo->getPromoEnd(),
            ":places" => $Promo->getPromoPlaces(),
            ":id" => $Promo->getPromoId()
        ]);

        $selectSql = "SELECT * FROM ".PREFIXE."Promo WHERE Promo_Id = :id";
        $selectStatement = $this->DB->prepare($selectSql);
        $selectStatement->execute([
            ":id" => $Promo->getPromoId()
        ]);
        return $selectStatement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function deletePromo(int $id) {
        $sql = "DELETE FROM ".PREFIXE."Contient WHERE Promo_Id = :id;
                DELETE FROM ".PREFIXE."Lecon WHERE Promo_Id = :id;
                DELETE FROM ".PREFIXE."Promo WHERE Promo_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":id"=> $id
        ]);
        return $retour;
    }

    public function allUserOfThisPromo(int $id):array {
        $sql = "SELECT ".PREFIXE."Promo.Promo_Name, ".PREFIXE."User.User_Id, ".PREFIXE."User.User_FirstName, ".PREFIXE."User.User_LastName, ".PREFIXE."User.User_Email, ".PREFIXE."User.User_Activate, ".PREFIXE."Role.Role_Name
                FROM ".PREFIXE."Promo
                INNER JOIN ".PREFIXE."Contient ON ".PREFIXE."Promo.Promo_Id = ".PREFIXE."Contient.Promo_Id
                INNER JOIN ".PREFIXE."User ON ".PREFIXE."Contient.User_Id = ".PREFIXE."User.User_Id
                INNER JOIN ".PREFIXE."Role ON ".PREFIXE."User.Role_Id = ".PREFIXE."Role.Role_Id
                WHERE ".PREFIXE."Promo.Promo_Id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([
            ":id" => $id
        ]);
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}


