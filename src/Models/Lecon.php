<?php

namespace src\Models;

use DateTime;
use src\Services\Hydratation;

class Lecon {

    private int $LeconId;
    private DateTime $LeconStart;
    private DateTime $LeconEnd;

    use Hydratation;

    /**
     * Get the value of LeconId
     */
    public function getLeconId(): int
    {
        return $this->LeconId;
    }

    /**
     * Set the value of LeconId
     */
    public function setLeconId(int $LeconId): self
    {
        $this->LeconId = $LeconId;

        return $this;
    }

    /**
     * Get the value of LeconStart
     */
    public function getLeconStart(): string
    {
        return $this->LeconStart->format('Y-m-d');
    }

    /**
     * Set the value of LeconStart
     */
    public function setLeconStart(string|DateTime $LeconStart): void
    {
        if($LeconStart instanceof DateTime) {
            $this->LeconStart = $LeconStart;
        } else {
            $this->LeconStart = new DateTime($LeconStart);
        }
    }

    /**
     * Get the value of LeconEnd
     */
    public function getLeconEnd(): string
    {
        return $this->LeconEnd->format('Y-m-d');
    }

    /**
     * Set the value of LeconEnd
     */
    public function setLeconEnd(string|DateTime $LeconEnd): void
    {
        if($LeconEnd instanceof DateTime) {
            $this->LeconEnd = $LeconEnd;
        } else {
            $this->LeconEnd = new DateTime($LeconEnd);
        }
    }
}