<?php

namespace src\Models;

use PDO;
use PDOException;

final class Database {

    private $DB;
    private $Config;

    public function __construct() {
        $this->Config = __DIR__.'/../../config.php';
        require_once $this->Config;
        $this->ConnexionDB();
    }

    private function ConnexionDB(){
        try {
            $dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME;
            $this->DB = new PDO($dsn, DB_USER, DB_PWD);
        } catch (PDOException $error) {
            echo "Quelque chose s'est mal passé : " . $error->getMessage();
        }
    }

    public function getDB() {
        return $this->DB;
    }

    public function initializeDB(): string {

        if ($this->testIfTablePromoExist()) {
            return "La base de données semble déjà remplie.";
            die();
        }
        try {
            $sql = file_get_contents(__DIR__.'/../Migrations/appligestion.sql');
            $this->DB->query($sql);
            if ($this->MiseAJourConfig()) {
                return "installation de la Base de Données terminée !";
                die();
              }
        } catch (PDOException $erreur) {
            return "impossible de remplir la Base de données : " . $erreur->getMessage();
            die();
          }
    }

    private function testIfTablePromoExist(): bool {
        $exist = $this->DB->query('SHOW TABLES FROM ' . DB_NAME . ' LIKE \'' . PREFIXE . 'Promo\'')->fetch();
        if ($exist !== false && $exist[0] == PREFIXE . "Promo") {
            return true;
        } else {
            return false;
        }
    }

    private function MiseAJourConfig(): bool {
        $fconfig = fopen($this->Config, 'w');
        $write = "
            <?php
            define('DB_HOST', '" . DB_HOST . "');
            define('DB_NAME', '" . DB_NAME . "');
            define('DB_USER', '" . DB_USER . "');
            define('DB_PWD', '" . DB_PWD . "');
            define('PREFIXE', '" . PREFIXE . "');
            define('HOME_URL', '". HOME_URL ."');
            define('DB_INITIALIZED', TRUE);";
        if (fwrite($fconfig, $write)) {
            fclose($fconfig);
            return true;
        } else {
            fclose($fconfig);
            return false;
        }
    }
}