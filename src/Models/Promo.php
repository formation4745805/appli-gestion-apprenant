<?php

namespace src\Models;

use DateTime;
use src\Services\Hydratation;

class Promo {

    private int $PromoId;
    private string $PromoName;
    private DateTime $PromoStart;
    private DateTime $PromoEnd;
    private int $PromoPlaces;

    use Hydratation;

    /**
     * Get the value of PromoId
     */
    public function getPromoId(): int
    {
        return $this->PromoId;
    }

    /**
     * Set the value of PromoId
     */
    public function setPromoId(int $PromoId): self
    {
        $this->PromoId = $PromoId;

        return $this;
    }

    /**
     * Get the value of PromoName
     */
    public function getPromoName(): string
    {
        return $this->PromoName;
    }

    /**
     * Set the value of PromoName
     */
    public function setPromoName(string $PromoName): self
    {
        $this->PromoName = $PromoName;

        return $this;
    }

    /**
     * Get the value of PromoStart
     */
    public function getPromoStart(): string
    {
        return $this->PromoStart->format('Y-m-d');
    }

    /**
     * Set the value of PromoStart
     */
    public function setPromoStart(string|DateTime $PromoStart): void
    {
        if($PromoStart instanceof DateTime) {
            $this->PromoStart = $PromoStart;
        } else {
            $this->PromoStart = new DateTime($PromoStart);
        }
    }

    /**
     * Get the value of PromoEnd
     */
    public function getPromoEnd(): string
    {
        return $this->PromoEnd->format('Y-m-d');
    }

    /**
     * Set the value of PromoEnd
     */
    public function setPromoEnd(string|DateTime $PromoEnd): void
    {
        if($PromoEnd instanceof DateTime) {
            $this->PromoEnd = $PromoEnd;
        } else {
            $this->PromoEnd = new DateTime($PromoEnd);
        }
    }

    /**
     * Get the value of PromoPlaces
     */
    public function getPromoPlaces(): int
    {
        return $this->PromoPlaces;
    }

    /**
     * Set the value of PromoPlaces
     */
    public function setPromoPlaces(int $PromoPlaces): self
    {
        $this->PromoPlaces = $PromoPlaces;

        return $this;
    }
}