<?php

namespace src\Models;

use src\Services\Hydratation;

class Role {

    private int $RoleId;
    private string $RoleName;

    use Hydratation;

    /**
     * Get the value of RoleId
     */
    public function getRoleId(): int
    {
        return $this->RoleId;
    }

    /**
     * Set the value of RoleId
     */
    public function setRoleId(int $RoleId): self
    {
        $this->RoleId = $RoleId;

        return $this;
    }

    /**
     * Get the value of RoleName
     */
    public function getRoleName(): string
    {
        return $this->RoleName;
    }

    /**
     * Set the value of RoleName
     */
    public function setRoleName(string $RoleName): self
    {
        $this->RoleName = $RoleName;

        return $this;
    }
}