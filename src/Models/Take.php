<?php

namespace src\Models;

use src\Services\Hydratation;

class Take {

    private int $LeconId;
    private int $UserId;
    private bool $TakeLate;
    private bool $TakeAbsent;

    use Hydratation;

    /**
     * Get the value of UserId
     */
    public function getUserId(): int
    {
        return $this->UserId;
    }

    /**
     * Set the value of UserId
     */
    public function setUserId(int $UserId): self
    {
        $this->UserId = $UserId;

        return $this;
    }

    /**
     * Get the value of LeconId
     */
    public function getLeconId(): int
    {
        return $this->LeconId;
    }

    /**
     * Set the value of LeconId
     */
    public function setLeconId(int $LeconId): self
    {
        $this->LeconId = $LeconId;

        return $this;
    }

    /**
     * Get the value of TakeAbsent
     */
    public function isTakeAbsent(): bool
    {
        return $this->TakeAbsent;
    }

    /**
     * Set the value of TakeAbsent
     */
    public function setTakeAbsent(bool $TakeAbsent): self
    {
        $this->TakeAbsent = $TakeAbsent;

        return $this;
    }

    /**
     * Get the value of TakeLate
     */
    public function isTakeLate(): bool
    {
        return $this->TakeLate;
    }

    /**
     * Set the value of TakeLate
     */
    public function setTakeLate(bool $TakeLate): self
    {
        $this->TakeLate = $TakeLate;

        return $this;
    }
}