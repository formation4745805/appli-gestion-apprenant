<?php

namespace src\Models;

use src\Services\Hydratation;

class User {

    private int $UserId;
    private string $UserLastName;
    private string $UserFirstName;
    private string $UserEmail;
    private bool $UserActivate = false;
    private string $UserPassword = "password";
    private int $RoleId = 6;

    use Hydratation;

    /**
     * Get the value of UserId
     */
    public function getUserId(): int
    {
        return $this->UserId;
    }

    /**
     * Set the value of UserId
     */
    public function setUserId(int $UserId): self
    {
        $this->UserId = $UserId;

        return $this;
    }

    /**
     * Get the value of UserPassword
     */
    public function getUserPassword(): string
    {
        return $this->UserPassword;
    }

    /**
     * Set the value of UserPassword
     */
    public function setUserPassword(string $UserPassword): self
    {
        $this->UserPassword = $UserPassword;

        return $this;
    }

    /**
     * Get the value of UserFirstName
     */
    public function getUserFirstName(): string
    {
        return $this->UserFirstName;
    }

    /**
     * Set the value of UserFirstName
     */
    public function setUserFirstName(string $UserFirstName): self
    {
        $this->UserFirstName = $UserFirstName;

        return $this;
    }

    /**
     * Get the value of UserLastName
     */
    public function getUserLastName(): string
    {
        return $this->UserLastName;
    }

    /**
     * Set the value of UserLastName
     */
    public function setUserLastName(string $UserLastName): self
    {
        $this->UserLastName = $UserLastName;

        return $this;
    }

    /**
     * Get the value of UserActivate
     */
    public function isUserActivate(): bool
    {
        return $this->UserActivate;
    }

    /**
     * Set the value of UserActivate
     */
    public function setUserActivate(bool $UserActivate): self
    {
        $this->UserActivate = $UserActivate;

        return $this;
    }

    /**
     * Get the value of UserEmail
     */
    public function getUserEmail(): string
    {
        return $this->UserEmail;
    }

    /**
     * Set the value of UserEmail
     */
    public function setUserEmail(string $UserEmail): self
    {
        $this->UserEmail = $UserEmail;

        return $this;
    }

    /**
     * Get the value of RoleId
     */
    public function getRoleId(): int
    {
        return $this->RoleId;
    }

    /**
     * Set the value of RoleId
     */
    public function setRoleId(int $RoleId): self
    {
        $this->RoleId = $RoleId;

        return $this;
    }
}