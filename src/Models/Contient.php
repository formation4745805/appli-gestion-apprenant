<?php

namespace src\Models;

use src\Services\Hydratation;

class Contient {

    private int $UserId;
    private int $PromoId;

    use Hydratation;

    /**
     * Get the value of UserId
     */
    public function getUserId(): int
    {
        return $this->UserId;
    }

    /**
     * Set the value of UserId
     */
    public function setUserId(int $UserId): self
    {
        $this->UserId = $UserId;

        return $this;
    }

    /**
     * Get the value of PromoId
     */
    public function getPromoId(): int
    {
        return $this->PromoId;
    }

    /**
     * Set the value of PromoId
     */
    public function setPromoId(int $PromoId): self
    {
        $this->PromoId = $PromoId;

        return $this;
    }
}